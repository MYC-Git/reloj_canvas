 function x2(n, i, x1, r) {
 	return x1 + r * Math.sin(2 * Math.PI * n / i);
 };

 function y2(n, i, y1, r) {
 	return y1 - r * Math.cos(2 * Math.PI * n / i);
 };

 function myLine(ctx, x1, y1, x2, y2, width, color, sobl, soxl, socl, cap) {
 	ctx.beginPath(); // comenzar nueva linea
 	ctx.moveTo(x1, y1); // Comienzo de linea
 	ctx.lineTo(x2, y2); // Final de linea
 	ctx.shadowBlur = sobl; //Difuminado de la sombra
 	ctx.shadowOffsetX = soxl; //Distancia horizontal de la sombra
 	ctx.shadowColor = socl; //Dolor de la sombra
 	ctx.strokeStyle = color; // color de línea
 	ctx.lineWidth = width; // anchura de linea:  5 puntos
 	ctx.stroke(); // dibujar linea
 	ctx.lineCap = cap; //Finales de líneas redondeados
 }

 function myCircle(ctx, x, y, r, width, color, re, sob, sox, soc) {
 	ctx.beginPath(); // comenzar figura // añadir arco (circulo completo):
 	ctx.arc(x, y, r, 0, 2 * Math.PI); //     ctx.arc(x, y, r, start, stop)
 	ctx.strokeStyle = color; // color de la línea del circulo
 	ctx.lineWidth = width; // anchura de la línea del circulo
 	ctx.fillStyle = re; //color de relleno
 	ctx.shadowBlur = sob; //difuminado de la sombra
 	ctx.shadowOffsetX = sox; //distancia horizontal de la sombra
 	ctx.shadowColor = soc; //color de la sombra
 	ctx.stroke(); // dibujar circulo
 	ctx.fill(); //dibuja el relleno
 }

 function mostrar_hora(ctx, a, b) {
 	var d = new Date();
 	var dia = d.getDate();
 	var dmes = d.getDay();
 	var h = d.getHours();
 	var m = d.getMinutes();
 	var s = d.getSeconds();
 	var ds = d.getMilliseconds();
 	var segundos = s;
 	var minutos = m;
 	var horas = h;
 	var diam = {
 		0: "Dom ",
 		1: "Lun ",
 		2: "Mar ",
 		3: "Mier ",
 		4: "Jue ",
 		5: "Vier ",
 		6: "Sab "
 	};
 	var hoy = diam[dmes];

 	if ((h === a) && (m === b && s < 5)) {
 		$('#carrillon')[0].play();
 	}

 	if (dia < 10) {
 		dia = "0" + dia;
 	}
 	if (segundos < 10) {
 		segundos = "0" + segundos;
 	}
 	if (minutos < 10) {
 		minutos = "0" + minutos;
 	}
 	if (horas < 10) {
 		horas = "0" + horas;
 	}
 	$('#tex').html(horas + ":" + minutos + ":" + segundos);
 	ctx.clearRect(0, 0, 250, 250) // borrar CANVAS
 	var dgr = ctx.createRadialGradient(150, 100, 1, 150, 30, 100); //Degradado radial para la esfera del reloj
 	dgr.addColorStop(0, "red");
 	dgr.addColorStop(1, "black");
 	var dgr2 = ctx.createRadialGradient(150, 100, 1, 150, 100, 22); //Degradado radial para la esfera de décimas
 	dgr2.addColorStop(0, "red");
 	dgr2.addColorStop(1, "black");
 	myCircle(ctx, 150, 70, 50, 3, "black", dgr, 10, 0, "white"); // Esfera del reloj
 	myCircle(ctx, 150, 100, 10, 1, "black", dgr2, 0.1, 0.1, "black"); //Esfera de décimas
 	myCircle(ctx, 150, 100, 1, 1, "black", "black", 0.1, 0.1, "black"); //tuerca
 	ctx.font = "1em digital";
 	ctx.fillText(horas + ":" + minutos + ":" + segundos, 125, 50); //reloj en texto

 	ctx.fillStyle = "white"; //Rectángulo para el dia del mes y de semana
 	ctx.fillRect(173, 65, 26, 8);
 	ctx.strokeRect(173, 65, 26, 8);
 	ctx.fillStyle = "black";
 	ctx.font = "0.5em digital";
 	ctx.fillText(hoy + dia, 174, 72);

 	myLine(ctx, 150, 100, x2(ds, 1000, 150, 10), y2(ds, 1000, 100, 10), 1, "black", 10, 5, "white", "round"); //décimas
 	myLine(ctx, 150, 70, x2(h, 12, 150, 30), y2(h, 12, 70, 30), 5, "grey", 10, 6, "black", "round"); // horas
 	myLine(ctx, 150, 70, x2(m, 60, 150, 40), y2(m, 60, 70, 40), 3, "grey", 10, 7, "black", "round"); // min.
 	myLine(ctx, 150, 70, x2(s, 60, 150, 50), y2(s, 60, 70, 50), 1, "red", 10, 8, "black", "round"); // seg.
 	myCircle(ctx, 150, 70, 2, 1, "black", "white", 0.1, 0.1, "black"); //Tuerca esfera
 }
 $(function() {
 	var c = document.getElementById("myCanvas"); // obtiene CANVAS
 	var a;
 	var b;
 	function crear() {
 		a = $('#inh').val();
 		b = $('#inm').val();
 		a = +a;
 		b = +b;
 	}
 	$('#hecho').on('click', crear);
 	$('#hecho').on('tap', crear);

 	if (c.getContext) { // CANVAS soportado?
 		var ctx = c.getContext("2d"); // define contexto 2D
 		mostrar_hora(ctx, a, b);
 		ctx.scale(3.5, 3.5);
 		setInterval(function() {
 			mostrar_hora(ctx, a, b);
 		}, 10)
 	}
 })