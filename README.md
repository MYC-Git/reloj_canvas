# Reloj con canvas
WebApp realizada como ejercicio opcional del módulo 9 de la 4ª edición del curso de la universidad politécnica de Madrid en Miriada x: Desarrollo en HTML5, CSS y Javascript de WebApps, incl. móviles FirefoxOS.  
Obtenga más [información](https://myc-git.gitlab.io/mycweb/paginas/javascript/rcanvas.html) o vealo en [funcionamiento](https://myc-git.gitlab.io/reloj_canvas/).